// Almada Canosa Agustin Nicolas. Libreta : 102/19.

#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();

#if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
#endif
    bool operator<(Fecha o);

  private:
    int dia_;
    int mes_;
};

Fecha::Fecha(int mes, int dia): dia_(dia), mes_(mes){}

int Fecha::mes(){
    return this -> mes_;
}

int Fecha::dia(){
    return this -> dia_;
}

void Fecha::incrementar_dia() {

    if(this->dia() < dias_en_mes(this->mes())) this ->dia_++;

    else if(this->mes() == 12){
        this->mes_=1;
        this->dia_=1;
    }

    else{
        this->mes_++;
        this->dia_=1;
    }

    return;

}

ostream& operator<<(ostream& os, Fecha f){
    os << f.dia() << "/" << f.mes();
    return os;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    // Completar iguadad (ej 9)
    return igual_dia && igual_mes;
}
#endif

bool Fecha::operator<(Fecha o) {
    bool menor_mes = this->mes() < o.mes();
    bool igual_mes = this->mes() == o.mes();
    bool menor_dia = this->dia() < o.dia();

    return menor_mes || (igual_mes && menor_dia);
}

// Ejercicio 11, 12

class Horario{
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator==(Horario o);
        bool operator<(Horario o);

    private:
        uint hora_;
        uint min_;

};

Horario::Horario(uint hora, uint min): hora_(hora), min_(min) {}

uint Horario::hora() {
    return this->hora_;
}

uint Horario::min() {
    return this -> min_;
}

ostream& operator<<(ostream& os, Horario h){
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator==(Horario o){
    bool misma_hora = this->hora() == o.hora();
    bool mismo_min = this->min() == o.min();

    return (misma_hora && mismo_min);
}

bool Horario::operator<(Horario o) {
    bool hora_menor = this->hora() < o.hora();
    bool hora_igual = this->hora() == o.hora();
    bool min_menor = this->min() < o.min();

    return hora_menor || (hora_igual && min_menor);
}

// Ejercicio 13

// Clase Recordatorio

class Recordatorio{
    public:
        Recordatorio(Fecha f, Horario h, string msg);
        Fecha fecha();
        Horario horario();
        string mensaje();
        bool operator==(Recordatorio o);
        bool operator<(Recordatorio o);

    private:
        Fecha fecha_;
        Horario horario_;
        string mensaje_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string msg) : fecha_(f), horario_(h), mensaje_(msg) {}

Fecha Recordatorio::fecha() {
    return this ->fecha_;
}

Horario Recordatorio::horario() {
    return this ->horario_;
}

string Recordatorio::mensaje() {
    return this -> mensaje_;
}

ostream& operator<<(ostream& os, Recordatorio r){
    os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
    return os;
}

bool Recordatorio::operator==(Recordatorio o) {
    bool fecha_igual = this -> fecha() == o.fecha();
    bool horario_igual = this -> horario() == o.horario();

    return fecha_igual && horario_igual;
}

bool Recordatorio::operator<(Recordatorio o) {
    bool fecha_menor = this -> fecha() < o.fecha();
    bool fecha_igual = this -> fecha() == o.fecha();
    bool horario_menor = this -> horario() < o.horario();

    return fecha_menor || (fecha_igual && horario_menor);
}

// Ejercicio 14

// Clase Agenda
class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

    private:
    Fecha fecha_;
    list<Recordatorio> recordatorios_;
};

Agenda::Agenda(Fecha fecha_inicial) : fecha_(fecha_inicial) {}

Fecha Agenda::hoy() {
    return this -> fecha_;
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    this -> recordatorios_.push_back(rec);
    return;
}

void Agenda::incrementar_dia() {
    this -> fecha_.incrementar_dia();
    return;
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> rec_de_hoy;

    for(Recordatorio r : this -> recordatorios_){
        if(r.fecha()== this -> hoy()) rec_de_hoy.push_back(r);
    }
    rec_de_hoy.sort();
    return rec_de_hoy;

}

ostream& operator<<(ostream& os, Agenda a){
    os << a.hoy() << endl;
    os << "=====" << endl;

    list<Recordatorio> recordatorios_de_hoy = a.recordatorios_de_hoy();

    for (Recordatorio r : recordatorios_de_hoy) {
        os << r << endl;
    }

    return os;
}