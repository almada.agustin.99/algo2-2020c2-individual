#include "Lista.h"

Lista::Lista() : head(NULL), tail(NULL){}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    // Completar
    Nodo* temp = this->head;
    this->tail = NULL;

    while (temp != NULL){
        temp = temp->next;
        delete(head);
        head = temp;
    }

    delete temp;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    // Completar
    Nodo* temp = aCopiar.tail;

    while(this->head!=NULL){
        this->eliminar(0);
    }

    while (temp != NULL){

        if(this -> head != NULL){

            Nodo* node = new Nodo();
            node->data = temp->data;
            node->next = this->head;
            this->head->prev = node;
            this->head = node;
        }

        else{
            Nodo* node = new Nodo();
            node->data = temp->data;
            node->next = NULL;
            node->prev = NULL;
            this->head = node;
            this->tail = node;

        }

        temp = temp->prev;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    // Completar
    Nodo* node = new Nodo();
    node->data = elem;
    node->next = this->head;
    node->prev = NULL;
    if(this->head == NULL){
        this->tail = node;
    }

    this->head = node;

    return;
}

void Lista::agregarAtras(const int& elem) {
    // Completar
    Nodo* node = new Nodo();
    node->data = elem;
    node->next = NULL;
    node->prev = this->tail;


    if(this->head == NULL){
        this->head = node;
    }
    else this->tail->next = node;

    this->tail = node;

    return;
}

void Lista::eliminar(Nat i) {
    // Completar
    if(this->longitud() <= i) return;

    Nodo* node =  this->head;
    for (int j = 0; j < i ; ++j) {
        node = node->next;
    }
    if(this->tail == node && this->head == node){
        this->head = NULL;
        this->tail = NULL;
    }

    else if(this->head == node){
        node->next->prev = NULL;
        this->head = node->next;
    }

    else if(this->tail == node){
        node->prev->next = NULL;
        this->tail = node->prev;
    }
    else{
        node->prev->next = node->next;
        node->next->prev = node->prev;
    }

    delete node;
    return;

}

int Lista::longitud() const {
    // Completar
    int i = 0;
    Nodo* temp = this->head;

    while(temp != NULL){
        i++;
        temp = temp->next;
    }
    return i;
}

const int& Lista::iesimo(Nat i) const {
    // Completar
    Nodo* temp = this->head;
    for (int j = 0; j < i; ++j) {
        temp = temp->next;
    }

    return temp->data;
}

int& Lista::iesimo(Nat i) {
    // Completar (hint: es igual a la anterior...)
    Nodo* temp = this->head;
    for (int j = 0; j < i; ++j) {
        temp = temp->next;
    }

    return temp->data;
}

void Lista::mostrar(ostream& o) {
    // Completar
}
