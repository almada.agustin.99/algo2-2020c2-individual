
#include "Conjunto.h"

template <class T>
Conjunto<T>::Conjunto() : _raiz(NULL){

}

template <class T>
Conjunto<T>::~Conjunto() { 
    // Completar
    destruir(this->_raiz);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
        return pertenece(*this->_raiz, clave);
}

template <class T>
bool Conjunto<T>::pertenece(Nodo& nodo, const T& clave) const {
    if (&nodo != nullptr){
        if (nodo.valor == clave){
            return true;
        }
        else{
            if (nodo.valor > clave){
                return pertenece(*nodo.izq, clave);

            }
            else{
                return pertenece(*nodo.der, clave);
            }

        }
    }
    return false;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if (this->_raiz == NULL){
        Nodo* node = new Nodo(clave);
        this->_raiz = node;
        return;
    }
    if(pertenece(*this->_raiz, clave)) return;
    else{
        insertar(*this->_raiz, clave);
    }

    return;
}

template<class T>
void Conjunto<T>::insertar(Conjunto::Nodo& nodo, const T& clave) {
    if (nodo.valor > clave){
        if(nodo.izq == nullptr){
            Nodo* node = new Nodo(clave);
            nodo.izq = node;
            return;
        }
        else{
            insertar(*nodo.izq, clave);
        }
    }
    else{
        if(nodo.der == nullptr){
            Nodo* node = new Nodo(clave);
            nodo.der = node;
            return;
        }
        else{
            insertar(*nodo.der, clave);
        }
    }

    return;

}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if(this->_raiz == nullptr) return;
    else if(this->_raiz->valor == clave && this->_raiz->izq == nullptr && this->_raiz->der == nullptr){
        delete this->_raiz;
        this->_raiz = nullptr;
        return;
    }
    else{
        remover(nullptr, "", this->_raiz, clave);
    }
    return;
}

template <class T>
void Conjunto<T>::remover(Nodo* padre, string dir, Nodo* nodo, const T& clave){
    if (nodo->valor == clave){
        if(nodo->izq == nullptr && nodo->der == nullptr){
            delete nodo;
            if (dir == "izq") padre->izq = nullptr;
            else padre->der = nullptr;
        }
        else{
            if (nodo->der != nullptr){
                nodo->valor = minimo(*nodo->der);
                remover(nodo, "der", nodo->der, nodo->valor);
            }
            else{
                nodo->valor = maximo(*nodo->izq);
                remover(nodo, "izq", nodo->izq, nodo->valor);
            }
        }
    }
    else{
        if (nodo->valor > clave){
            remover(nodo, "izq", nodo->izq, clave);
        }
        else{
            remover(nodo, "der", nodo->der, clave);
        }
    }

    return;
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    if (this->_raiz == nullptr) return clave;
    else if(this->_raiz->izq == nullptr && this->_raiz->der == nullptr) return clave;
    if(this->_raiz->valor > clave && this->_raiz->izq != nullptr) return this->siguiente(this->_raiz, "izq", this->_raiz->izq, clave);
    else if(this->_raiz->valor < clave && this->_raiz->der != nullptr) return this->siguiente(this->_raiz, "der", this->_raiz->der, clave);

    return clave;

}

template <class T>
const T& Conjunto<T>::siguiente(Nodo* padre, string dir, Nodo* nodo, const T& clave) {
    if (nodo->valor == clave){
        if (nodo->der != nullptr){
            return minimo(*nodo->der);
        }
        else{
            if (dir == "izq") return padre->valor;
            if (dir == "der") return clave;
        }
    }
    else if(nodo->valor > clave){
        const T& valor = this->siguiente(nodo, "izq", nodo->izq, clave);
        if(valor != clave) return valor;
        else{
            return nodo->valor;
        }
    }
    else{
        const T& valor = this->siguiente(nodo, "der", nodo->izq, clave);
        if (valor != clave) return valor;
        else{
            return clave;
        }
    }

}

template <class T>
const T& Conjunto<T>::minimo() const {
    return minimo(*this->_raiz);
}

template <class T>
const T& Conjunto<T>::minimo(Nodo& nodo) const {
    if (&nodo == nullptr){
        return 0;
    }
    else{
        if (nodo.izq == nullptr){
            return nodo.valor;
        }
        else return minimo(*nodo.izq);
    }
}

template <class T>
const T& Conjunto<T>::maximo() const {
    return maximo(*this->_raiz);
}

template <class T>
const T& Conjunto<T>::maximo(Nodo& nodo) const {
    if (&nodo == nullptr){
        return 0;
    }
    else{
        if (nodo.der == nullptr){
            return nodo.valor;
        }
        else return maximo(*nodo.der);
    }
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return contar(this->_raiz);
}

template <class T>
int Conjunto<T>::contar(Nodo* nodo) const {
    if(nodo == nullptr) return 0;
    else if (nodo->izq == nullptr && nodo->der == nullptr) return 1;
    else{
        return 1 + contar(nodo->der) + contar(nodo->izq);
    }
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
}

template<class T>
void Conjunto<T>::destruir(Conjunto::Nodo* nodo) {
    if (nodo != nullptr){
        destruir(nodo->izq);
        destruir(nodo->der);
        delete nodo;
    }
    return;
}




