
#include "string_map.h"

template <typename T>
string_map<T>::string_map() : raiz(nullptr), _size(0){
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    // COMPLETAR
    if (d.raiz == nullptr){
        this->raiz == nullptr;
        this->_size = 0;
        return *this;
    }
    Nodo* node = new Nodo();
    this->raiz = node;
    copiar(*this->raiz, *d.raiz);
    return *this;

}

template<typename T>
void string_map<T>::copiar(string_map::Nodo &copia, const string_map::Nodo &aCopiar) {
    if (aCopiar.definicion != nullptr){
        T* elem = new T(*aCopiar.definicion);
        copia.definicion = elem;
        this->_size++;
    }

    for (int i = 0; i < aCopiar.siguientes.size(); ++i) {
        if (aCopiar.siguientes[i] != nullptr){
            Nodo* node = new Nodo();
            copia.siguientes[i] = node;
            copiar(*node, *aCopiar.siguientes[i]);
        }
    }


}


template <typename T>
string_map<T>::~string_map(){
    if (this->raiz == nullptr) return;
    destruir(*this->raiz);
    delete this->raiz;
    return;

}

template<typename T>
void string_map<T>::destruir(string_map::Nodo& nodo) {
    for(int i = 0; i < nodo.siguientes.size(); i++){
        if (nodo.siguientes[i] != nullptr){
            destruir(*nodo.siguientes[i]);
            if (nodo.siguientes[i]->definicion != nullptr) delete nodo.siguientes[i]->definicion;
            delete nodo.siguientes[i];
            nodo.siguientes[i] = nullptr;
        }
    }
    return;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    // COMPLETAR
    if (this->raiz == nullptr) return 0;

    return count(clave, *this->raiz, 0);
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    return at(clave, *this->raiz, 0);
}

template<typename T>
const T &string_map<T>::at(const string &key, string_map::Nodo& nodo, int index) const {
    if (index == key.length()-1){
        return *nodo.siguientes[int(key[index])]->definicion;
    }
    else{
        return at(key, *nodo.siguientes[int(key[index])], index+1);
    }
}


template <typename T>
T& string_map<T>::at(const string& clave) {
    return at(clave, *this->raiz, 0);
}


template<typename T>
T &string_map<T>::at(const string &key, string_map::Nodo& nodo, int index) {
    if (index == key.length()-1){
        return *nodo.siguientes[int(key[index])]->definicion;
    }
    else{
        return at(key, *nodo.siguientes[int(key[index])], index+1);
    }
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    // COMPLETAR
    erase(clave, *this->raiz, 0, *this->raiz, 0, false);
    return;
}

template<typename T>
void string_map<T>::erase(const string &key, string_map::Nodo& nodo, int index, string_map::Nodo& ultNodo, int ultIndex, bool yaBorro) {
    if(index == key.length()-1){
        if(cantidadHijos(*nodo.siguientes[int(key[index])]) == 0){
            if (yaBorro == true){
                delete nodo.siguientes[int(key[index])]->definicion;
                delete nodo.siguientes[int(key[index])];
                this->_size--;
                return;
            }
            erase(key, ultNodo, ultIndex, ultNodo, ultIndex, true);
        }
        else{
            delete nodo.siguientes[int(key[index])]->definicion;
            nodo.siguientes[int(key[index])]->definicion = nullptr;
            return;
        }
    }
    else{
        if (cantidadHijos(*nodo.siguientes[int(key[index])]) > 1 ||  nodo.siguientes[int(key[index])]->definicion != nullptr){
            erase(key, *nodo.siguientes[int(key[index])], index+1, *nodo.siguientes[int(key[index])], index+1, false);
        }
        else{
            if (yaBorro == true){;
                erase(key, *nodo.siguientes[int(key[index])], index+1, ultNodo, ultIndex, true);
                delete nodo.siguientes[int(key[index])];
                nodo.siguientes[int(key[index])] = nullptr;
            }

            else{
                erase(key, *nodo.siguientes[int(key[index])], index+1, ultNodo, ultIndex, false);
            }

        }
    }
}

template <typename T>
int string_map<T>::size() const{
    return this->_size;
}

template <typename T>
bool string_map<T>::empty() const{
    if (this->raiz == nullptr || (this->raiz->definicion == nullptr && cantidadHijos(*this->raiz) == 0)){
        return true;
    }
    return false;
}

template<typename T>
void string_map<T>::insert(const pair<string, T>& tupla) {
    //COMPLETAR
    if (this->raiz == nullptr){
        Nodo* node = new Nodo();
        this->raiz = node;
    }
    insert(tupla, *this->raiz, 0);
    return;

}

template<typename T>
void string_map<T>::insert(const pair<string, T>& tupla, string_map::Nodo& nodo, int index) {
    if (nodo.siguientes[int(tupla.first[index])] == nullptr){
        if (index == tupla.first.length() - 1){
            this->_size++;
            T* elem = new T(tupla.second);
            Nodo* node = new Nodo(elem);
            nodo.siguientes[int(tupla.first[index])] = node;
        }
        else{
            Nodo* node = new Nodo();
            nodo.siguientes[int(tupla.first[index])] = node;
            insert(tupla, *node, index+1);
        }
    }
    else{
        if(index == tupla.first.length()-1){
            if (nodo.siguientes[int(tupla.first[index])]->definicion == nullptr) this->_size++;
            else delete nodo.siguientes[int(tupla.first[index])]->definicion;
            T* elem = new T(tupla.second);
            nodo.siguientes[int(tupla.first[index])]->definicion = elem;
        }
        else{
            insert(tupla, *nodo.siguientes[int(tupla.first[index])], index+1);
        }
    }
    return;
}

template<typename T>
int string_map<T>::count(const string& clave, string_map::Nodo& nodo, int index) const{
    if (index == clave.length()-1){
        if(nodo.siguientes[int(clave[index])]->definicion == nullptr) return 0;
        else return 1;
    }
    else{
        if(nodo.siguientes[int(clave[index])] == nullptr) return 0;
        else{
            return count(clave, *nodo.siguientes[int(clave[index])], index+1);
        }
    }
}

template<typename T>
const int string_map<T>::cantidadHijos(string_map::Nodo& nodo) const{
    int hijos = 0;
    for (int i = 0; i < nodo.siguientes.size(); i++){
        if (nodo.siguientes[i] != nullptr) hijos++;
    }
    return hijos;
}







